# Description

This is the [mustache](https://mustache.github.io/mustache.5.html) template I created for this website: [https://cl.lingfil.uu.se/~marie/](https://cl.lingfil.uu.se/~marie/).
It is based on the template of [Xiaoying Riley](https://themes.3rdwavemedia.com/bootstrap-templates/resume/free-bootstrap-theme-for-web-developers/).

# Installation
1) Download this folder and cd in it.
create local environement: 
2) `python3 -m venv mus`
3) `source mus/bin/activate`

4) `pip install -r requirements.txt`

# Usage:
1) cd to this folder
2) source `mus/bin/activate`
3) sh `render.sh`
4) Start modifying either base.html (structure) either index.yml (data)
5) in order to keep the website upgrading while modifying do:
`while true ;do sh render.sh ; sleep 10; done `